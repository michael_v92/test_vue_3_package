const webpack = require('webpack');

module.exports = {
  devServer: {
    port: 7091,
  },
  configureWebpack: {
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
    ],
  }
};
