module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': 'warn',
    'no-debugger': 'warn',
    'import/prefer-default-export': 'off',
    'max-len': ["error", 100]
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/**/*.spec.{j,t}s?(x)'],
      rules: { 'no-console': 'off' },
      env: {
        jest: true,
      },
    },
    {
      files: ['**/icons/*.vue'],
      rules: { 'vue/max-attributes-per-line': 'off', 'max-len': 'off' },
    },
  ],
};
