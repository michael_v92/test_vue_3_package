import { createApp } from 'vue';

import App from './playground/index.vue';

import './scss/app.scss';

const app = createApp(App);

app.mount('#app');
