# DsIcon

Отрисовка иконок

##### Props

`*` - required

Name | Description
---------|---------------
name* | Имя иконки находящейся в `src/icons`
icon-size | Размер иконки: 'none', S', 'M', 'L', 'XL', <br> default: 'none'

###### # Example
```vue
<template>
  <ds-icon class="icon" :name="icon" />
</template>

<script>
export default {
  data: () => ({
    links: [
      { type: 'link' },
      { type: 'divider' },
      { type: 'link' },
    ],
  }),
}
</script>
```
