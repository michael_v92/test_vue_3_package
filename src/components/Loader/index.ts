import { App } from 'vue';
import Loader from './index.vue';

Loader.install = (Vue: App) => {
  Vue.component(Loader.name, Loader);
};

export default Loader;
