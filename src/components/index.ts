import { App } from 'vue';

import Icon from './Icon';
import Loader from './Loader';

import '../scss/app.scss';

const components = [
  Icon,
  Loader,
];

function install(Vue: App) {
  components.forEach((component) => {
    Vue.component(component.name, component);
  });
}

export default { install };

export {
  Icon,
  Loader,
};
