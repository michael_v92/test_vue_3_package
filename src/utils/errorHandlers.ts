/* eslint-disable no-console */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const errorLogger = (message: string, errorObject: any = {}) => {
  console.error(message);

  if (Object.keys(errorObject).length) {
    console.log({ error: errorObject });
  }
};

export {
  // eslint-disable-next-line import/prefer-default-export
  errorLogger,
};
