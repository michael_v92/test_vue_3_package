module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript',
  transform: {
    '^.+\\.vue$': 'vue-jest',
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: [
    '**/*.{ts,vue}',
    '!**/node_modules/*',
    '!**/src/**/*.spec.ts',
  ],
  testMatch: ['<rootDir>/src/**/*.spec.ts'],
};
